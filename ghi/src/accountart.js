import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import './accountart.css';

const AccountArt = () => {
  const [userArt, setUserArt] = useState([]);
  const [selectedSize, setSelectedSize] = useState('16x16');

  useEffect(() => {
    const fetchUserArt = async () => {
      try {
        // Fetch user's art based on the selected size
        const authToken = localStorage.getItem('yourAuthToken');
        if (!authToken) {
          console.error('Authentication token not found');
          return;
        }

        const decodedToken = JSON.parse(atob(authToken.split('.')[1]));
        const { account: { account_id } } = decodedToken;

        const userArtResponse = await fetch(`http://localhost:8000/api/userart/${account_id}`);
        if (userArtResponse.ok) {
          const userArtData = await userArtResponse.json();
          setUserArt(userArtData);
        } else {
          console.error('Failed to fetch user art');
        }
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchUserArt();
  }, [selectedSize]); // Trigger fetchUserArt when selectedSize changes

  const handleSizeChange = (event) => {
    setSelectedSize(event.target.value);
  };

  const renderArtBySize = () => {
    return userArt
      .filter(art => art.size === selectedSize)
      .map((art, index) => (
        <div key={index} className="new-pixel-art-item">
          <Link to={`/pixelart/${art.art_id}`}>
            <h4>{art.name}</h4>
          </Link>
          <p>Creation Date: {new Date(art.creation_date).toLocaleDateString()}</p>
          <div className={`new-pixel-grid-${selectedSize}`}>
            {art.pixel_data.map((row, rowIndex) => (
              <div key={rowIndex} className="new-pixel-row">
                {row.map((color, colIndex) => (
                  <div
                    key={colIndex}
                    className="new-pixel"
                    style={{ backgroundColor: color || '#6F4E37' }}
                  ></div>
                ))}
              </div>
            ))}
          </div>
        </div>
      ));
  };

  return (
    <div>
      <h2>Your Pixel Art Gallery</h2>
      <select value={selectedSize} onChange={handleSizeChange}>
        <option value="16x16">16x16</option>
        <option value="32x32">32x32</option>
        <option value="64x64">64x64</option>
      </select>
      {renderArtBySize()}
    </div>
  );
};

export default AccountArt;