import React, { useState, useEffect } from 'react';
import { ChromePicker } from 'react-color';

import 'bootstrap/dist/css/bootstrap.min.css'; // Import Bootstrap CSS
import './CreatePixelArt.css'; // Import custom CSS

const CreatePixelArt = () => {
  const createEmptyGrid = (rows, cols) => {
    return Array.from({ length: rows }, () => Array(cols).fill('#FFFFFF'));
  };

  const [name, setName] = useState('');
  const [size, setSize] = useState('16x16');
  const [grid, setGrid] = useState(createEmptyGrid(16, 16));
  const [selectedColor, setSelectedColor] = useState('#000000');
  const [drawing, setDrawing] = useState(false);
  const [lastClickedSquare, setLastClickedSquare] = useState(null);

  useEffect(() => {
    const [rows, cols] = size.split('x').map(Number);
    setGrid(createEmptyGrid(rows, cols));
    handleGridSizeChange(size); // Call handleGridSizeChange whenever size changes
  }, [size]);

  const handleColorChange = (row, col) => {
    if (drawing) {
      const newGrid = grid.map(row => row.slice()); // Create a new copy of the grid
      newGrid[row][col] = selectedColor;
      setGrid(newGrid);
    }
  };

  const handleMouseDown = (row, col) => {
    setDrawing(true);
    setLastClickedSquare({ row, col });
  };

  const handleMouseUp = () => {
    setDrawing(false);
    setLastClickedSquare(null);
  };

  const handlePixelClick = (row, col) => {
    const newGrid = grid.map(row => row.slice()); // Create a new copy of the grid
    newGrid[row][col] = selectedColor;
    setGrid(newGrid);
    setLastClickedSquare({ row, col });
  };

  const getUserId = () => {
    const token = localStorage.getItem('yourAuthToken');

    if (token) {
      try {
        // Decode the JWT token
        const decodedToken = JSON.parse(atob(token.split('.')[1]));

        // Assuming the decoded token structure includes the account_id
        const account_id = decodedToken.account?.account_id;
        if (account_id) {
          return account_id;
        } else {
          console.error('Account ID not found in decoded token:', decodedToken);
          return null;
        }
      } catch (error) {
        console.error('Error decoding token:', error);
        return null;
      }
    }

    return null;
  };

  const handleCreatePixelArt = async () => {
    try {
      const account_id = getUserId();
      console.log('Account ID:', account_id);

      // Convert the grid into a format suitable for the server
      const formattedPixelData = grid.map(row => row.map(color => color));

      console.log('Formatted Pixel Data:', formattedPixelData);
      console.log('Request Payload:', JSON.stringify({
        account_id,
        pixel_data: formattedPixelData,
        name,
        size,
      }));

      const response = await fetch('http://localhost:8000/api/pixel_art', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          account_id,
          pixel_data: formattedPixelData,
          name,
          size,
        }),
      });

      console.log('Server Response:', response);

      if (!response.ok) {
        const data = await response.json();
        console.error('Error details:', data); // Log the entire error object
        alert(`Error: ${response.status} - ${response.statusText}`);
      } else {
        alert('Pixel art created successfully!');
      }
    } catch (error) {
      console.error('Error creating pixel art:', error);
    }
  };

  const handleGridSizeChange = (size) => {
    if (size === '32x32') {
      // Adjust pixel width and height for 32x32 grid
      document.documentElement.style.setProperty('--pixel-width', '20px');
      document.documentElement.style.setProperty('--pixel-height', '20px');
    } else if (size === '64x64') {
      // Adjust pixel width and height for 64x64 grid
      document.documentElement.style.setProperty('--pixel-width', '10px');
      document.documentElement.style.setProperty('--pixel-height', '10px');
    }
    // Adjust pixel width and height for other grid sizes as needed
  };

  return (
    <div className="container mt-5">
      <h1 className="mb-4">Create Pixel Art</h1>

      <div className="form-group">
        <label htmlFor="name">Name:</label>
        <input
          type="text"
          id="name"
          className="form-control"
          value={name}
          onChange={(e) => setName(e.target.value)}
          required
        />
      </div>

      <div className="form-group">
        <label htmlFor="size">Size:</label>
        <select
          id="size"
          className="form-control"
          value={size}
          onChange={(e) => setSize(e.target.value)}
          required
        >
          <option value="16x16">16x16</option>
          <option value="32x32">32x32</option>
        </select>
      </div>

      <div className="color-picker-container">
        {/* Color Picker */}
        <div className="form-group">
          <label htmlFor="color">Color:</label>
          <ChromePicker
            className="chrome-picker" // Assign a class name to the ChromePicker component
            color={selectedColor}
            onChange={(color) => setSelectedColor(color.hex)}
          />
        </div>
      </div>

      <div
        className={`pixel-grid pixel-grid-${size}`}
        style={{ gridTemplateColumns: `repeat(${grid[0].length}, 1fr)` }}
        onMouseDown={handleMouseDown}
        onMouseUp={handleMouseUp}
      >
        {grid.map((row, rowIndex) => (
          <div key={rowIndex} className="pixel-row">
            {row.map((color, colIndex) => (
              <div
                key={colIndex}
                className="pixels"
                style={{ backgroundColor: color || '#6F4E37'}}
                onMouseEnter={() => handleColorChange(rowIndex, colIndex)}
                onClick={() => handlePixelClick(rowIndex, colIndex)}
              ></div>
            ))}
          </div>
        ))}
      </div>

      <div className="button-container">
        <button className="btn btn-primary" onClick={handleCreatePixelArt}>Create Pixel Art</button>
      </div>
    </div>
  );
};

export default CreatePixelArt;