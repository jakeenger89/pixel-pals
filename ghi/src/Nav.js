import { NavLink, useNavigate } from "react-router-dom";
import React from 'react';
import './Nav.css';

function Nav({ isAuthenticated, onLogout }) {
  const navigate = useNavigate();
  const handleLogout = () => {
    localStorage.removeItem("yourAuthToken");
    if (onLogout) {
      onLogout();
    }
    navigate("/loginform");
    window.location.reload();
  };

  return (
    <div>
      <nav className="navbar">
        <div>
          <NavLink to="/" className="flex items-center justify-center mb-8">
            <img src="/path/to/logo.png" alt="Logo" className="h-8" />
          </NavLink>
          <ul className="list-none">
            <li className="mb-4">
              <NavLink to="/pixelartgallery">
                Pixel Art Gallery
              </NavLink>
            </li>
            {isAuthenticated && (
              <li className="mb-4">
                <NavLink to="/createpixelart">
                  Create Art
                </NavLink>
              </li>
            )}
            {isAuthenticated && (
              <li className="mb-4">
                <NavLink to="/account">
                  My Profile
                </NavLink>
              </li>
            )}
            {!isAuthenticated && (
              <li className="mb-4">
                <NavLink to="/signupform">
                  Sign Up
                </NavLink>
              </li>
            )}
            {!isAuthenticated && (
              <li className="mb-4">
                <NavLink to="/loginform">
                  Login
                </NavLink>
              </li>
            )}
            {isAuthenticated && (
              <li className="mb-4">
                <button onClick={handleLogout}>
                  Logout
                </button>
              </li>
            )}
          </ul>
        </div>
      </nav>
      <div className="content">
        {/* Your content goes here */}
      </div>
    </div>
  );
}

export default Nav;